#include "Game.h"
#include "TextureUtils.h"

#include "Player.h"
#include "NPC.h"
#include "Bullet.h"
#include "Vector2f.h"
#include "AABB.h"

//for printf
#include <cstdio>

// for exceptions
#include <stdexcept>

Game::Game() 
{
    gameWindow = nullptr;
    gameRenderer = nullptr;

    npcsspawncount = 20;

    backgroundTexture = nullptr;
    player = nullptr;
    maxScore = 1000;
    npcs.resize(npcsspawncount);

    keyStates = nullptr;

    npcsdeaths = 0;

    quit = false;

    currentdamagecounter = 0;
    maxdamagecounter = 1.0f;

    


}
void Game::startgame()
{
    printf("//////////////////////////////////////////////////////////////\n");
    printf("As purple boy, you find yourself in the graveyard again. The angry sound of the deadly flesh eaters resonotaes in the distance. there isn t much that you can do but fight for your life and save the village from a dreadful death. Shoot the flesh eaters and runaway so they don t catch you. your life depends on it! When you have deafeted the angry flesh eaters you will be able to return home safe.\n");
    printf("//////////////////////////////////////////////////////////////\n");

}

void Game::init()
{
     gameWindow = SDL_CreateWindow("BLOOD MOON",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags  
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          throw std::runtime_error("Error - SDL could not create renderer\n");          
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        throw std::runtime_error("Error - SDL could not create Window\n");
    }
    
    // Track Keystates array
	keyStates = SDL_GetKeyboardState(NULL);

    // Create background texture from file, optimised for renderer 
    backgroundTexture = createTextureFromFile("assets/images/untitled.png", gameRenderer);

    if(backgroundTexture == nullptr)
        throw std::runtime_error("Background image not found\n");

    //setup player
    player = new Player();
    player->init(gameRenderer);
    player->setGame(this);

    for (size_t i = 0; i < npcs.size(); i++)
    {
        npcs[i] = new NPC();
        npcs[i]->init(gameRenderer);
        npcs[i]->setGame(this);
        npcs[i]->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
    }
    
    //npc->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);

    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
    startgame();
}

Game::~Game()
{
    //Clean up!
    delete player;
    player = nullptr;  

    for (size_t i = 0; i < npcs.size(); i++)
    {
        delete npcs[i];
        npcs[i] = nullptr;
    }


   

    for (vector<Bullet*>::iterator it = bullets.begin() ; it != bullets.end();)
    {   
        delete *it;
        *it = nullptr;
        it = bullets.erase(it);
    } 
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   
}

void Game::draw()
{
     // 1. Clear the screen
    SDL_RenderClear(gameRenderer);

    // 2. Draw the scene
    SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);




    for (size_t i = 0; i < npcs.size(); i++)
    {
        npcs[i]->draw(gameRenderer);
    }



    player->draw(gameRenderer);
    

    for(int i = 0; i < bullets.size(); i++)
        bullets[i]->draw(gameRenderer);

    // 2.5 Draw HUD
   /* printf("Player Score: %d\n", player->getScore());*/

    //if (player->getScore() >= maxScore)
    //    /*printf("YOU WIN\n");*/

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    
}

void Game::update(float timeDelta)
{
    player->update(timeDelta);
    
    if (player->getHealth() <= 0)
    {
        printf("Game Over");
        quit = true;
    }





    for (size_t i = 0; i < npcs.size(); i++)
    {
        if (npcs[i]->isDead())
            npcs[i]->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);

        npcs[i]->update(timeDelta);
    }

    

    for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();)
    {
        if((*it)->hasExpired())
        {
            delete *it;
            *it = nullptr;
            it = bullets.erase(it);
        }
        else
        {
            ++it;
        }
    }

    for (vector<Bullet*>::iterator it = bullets.begin() ; it != bullets.end(); ++it)
    {
        (*it)->update(timeDelta);
    }

    collisionDetection();
    currentdamagecounter += timeDelta;

}

void Game::processInputs()
{
    SDL_Event event;

    // Handle input 
    if( SDL_PollEvent( &event ))  // test for events
    { 
        switch(event.type) 
        { 
            case SDL_QUIT:
                quit = true;
            break;

            // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    quit = true;
                    break;
                }
            break;

            // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    //  Nothing to do here.
                    break;
                }
            break;
            
            default:
                // not an error, there's lots we don't handle. 
                break;    
        }
    }

    // Process Inputs - for player 
    player->processInput(keyStates);
}

Player* Game::getPlayer() 
{
    return player;
}

void Game::runGameLoop()
{
    // Timing variables
    unsigned int currentTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;
    unsigned int prevTimeIndex;

    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist 
        //- https://gafferongames.com/post/fix_your_timestep/
        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001f;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Process inputs
        processInputs();

        // Update Game Objects
        update(timeDeltaInSeconds);

        //Draw stuff here.
        draw();

        SDL_Delay(1);
    }
}

void Game::createBullet(Vector2f* position, Vector2f* velocity)
{
    Bullet* bullet = new Bullet();

    bullet->init(gameRenderer, position, velocity);

    bullets.push_back(bullet);
}

void Game::collisionDetection()
{
    for (size_t i = 0; i < npcs.size(); i++)
    {
        for (vector<Bullet*>::iterator bullet = bullets.begin(); bullet != bullets.end();)
        {
            if (npcs[i]->getAABB() != nullptr)
            {
                if ((*bullet)->getAABB()->intersects(npcs[i]->getAABB()) && npcs[i]->state != npcs[i]->DEAD)
                {
                    // increase player score
                    player->addScore(npcs[i]->getPoints());

                    // damage npc
                    npcs[i]->takeDamage((*bullet)->getDamage());

                    // destroy arrow (or it'll keep hitting!)
                    delete* bullet;
                    *bullet = nullptr;
                    bullet = bullets.erase(bullet);
                }
                else
                {
                    ++bullet;
                }
            }

            
        }
        if (npcs[i]->getAABB()->intersects(player->getAABB()) && npcs[i]->state != npcs[i]->DEAD) 
        {
            if (currentdamagecounter >= maxdamagecounter)
            {
                player->takeHealth(20);
                


                currentdamagecounter = 0;
            }
        }
    }

    
}

void Game::ShowScore()
{
    if (player->getScore() < maxScore)
        printf("Player health: %d\n", player->getHealth());

    if (player->getScore() < maxScore)
        printf("Player Score: %d\n", player->getScore());
    else if (player->getScore() >= maxScore)
    {
        printf("YOU WIN\n");
        quit = true;
    }


}




void Game::createNPC(int spawncount) 

{
    npcs.clear();
    npcs.resize(spawncount);


    for (size_t i = 0; i < npcs.size(); i++)
    {
        npcs[i] = new NPC();
        npcs[i]->init(gameRenderer);
        npcs[i]->setGame(this);
        npcs[i]->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        npcs[i]->draw(gameRenderer);
    }
}

void Game::clearnpcs() 
{
    npcsdeaths = 0;
    for (size_t i = 0; i < npcs.size(); i++)
    {
        delete npcs[i];

        if (i = npcs.size())
        {
            npcsspawncount+= 5;
            createNPC(npcsspawncount);
        }
    }
}